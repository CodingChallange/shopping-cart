
<!DOCTYPE html>
<html lang="en">
<head>
    <title>ezyVet - @yield('title')</title>
    <meta charset="utf-8">
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
    >
    <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    >
    <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
        crossorigin="anonymous"
    >
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        .blue-bg {
            background: #1396c4 !important;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark blue-bg">
    <a
        class="navbar-brand"
        href="{{ route('products.index') }}"
    >ezyVet</a>
    <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#collapsibleNavbar"
    >
        <span class="navbar-toggler-icon"></span>
    </button>
    <div
        class="collapse navbar-collapse"
        id="collapsibleNavbar"
    >
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a
                    class="nav-link"
                    href="{{ route('products.index') }}"
                >Products</a>
            </li>

        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a
                    class="nav-link"
                    href="{{ route('cart.index') }}"
                >Cart <span class="fas fa-shopping-cart"></span></a>
            </li>
        </ul>
    </div>
</nav>

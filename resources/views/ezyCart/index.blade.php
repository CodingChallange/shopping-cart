@extends('ezyCart.layout')

@section('title', 'Products')

@section('content')
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Product name</th>
                <th>Price</th>
                <th>Link</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product['name']  }}</td>
                    <td>@include('ezyCart.partials.currency',['price'=>$product['price']])</td>
                    <td>
                        <a href="{{ route('cart.add', $product['name']) }}">Add to cart</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection

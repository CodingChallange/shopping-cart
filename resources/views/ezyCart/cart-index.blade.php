@extends('ezyCart.layout')

@section('title', 'Cart')

@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Product name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Link</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cartItems as $id=>$item)
            <tr>
                <td>{{ $item->name  }}</td>
                <td>@include('ezyCart.partials.currency',['price'=>$item->price])</td>
                <td>
                    {{  $item->quantity }}
                </td>
                <td>
                    @include('ezyCart.partials.currency',['price'=> $item->total ])
                </td>
                <td>
                    <a href="{{ route('cart.remove', $id) }}">remove</a>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td>Overall total</td>
            <td></td>
            <td></td>
            <td>@include('ezyCart.partials.currency',['price'=> $total ])</td>
            <td></td>
        </tr>
        </tfoot>
    </table>
@endsection

@include('ezyCart.partials.header')

<div
    class="container"
    style="margin-top:30px"
>
    @yield('content')
</div>

@include('ezyCart.partials.footer')

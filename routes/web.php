<?php

use App\Http\Controllers\EzyProductController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EzyProductController::class, 'index'])->name('products.index');

Route::get('cart', function () {
    return EzyShoppingCart::index();
})->name('cart.index');

Route::get('cart/add/{uniqueId}', function ($name) {
    return EzyShoppingCart::store($name);
})->name('cart.add');

Route::get('cart/remove/{uniqueId}', function ($name) {
    return EzyShoppingCart::delete($name);
})->name('cart.remove');

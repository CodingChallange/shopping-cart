<?php

namespace Tests\Unit;


use App\Ezy\EzyProduct;
use App\Ezy\ShoppingCart\Facades\EzyShoppingCart;
use Illuminate\Foundation\Testing\TestCase;
use Tests\CreatesApplication;

class CartTest extends TestCase
{
    use CreatesApplication;


    /** @test */
    public function add_a_product_to_the_cart()
    {
        $product = EzyProduct::first();
        $cartItem = EzyShoppingCart::add($product['name'], 1, $product['price']);

        $this->assertCount(5, $cartItem->toArray());
    }

    /** @test */
    public function check_the_product_structure()
    {
        $product = EzyProduct::first();
        EzyShoppingCart::add($product['name'], 1, $product['price']);

        $this->assertArrayStructure(['id','name','price','quantity','total'], [EzyShoppingCart::GetContent()->first()->toArray()]);
    }

    /** @test */
    public function check_the_cart_product_total()
    {
        $product = EzyProduct::first();
        $productQuantity = 2;
        EzyShoppingCart::add($product['name'], $productQuantity, $product['price']);

        $this->assertEquals($productQuantity * $product['price'], EzyShoppingCart::GetContent()->first()->total);
    }

    /** @test */
    public function check_the_cart_overall_total()
    {
        $products = EzyProduct::all();
        $productQuantity = 2;
        $overallTotal = 0;

        $productFirst = $products->splice(1,1)->first();
        $productSecond = $products->splice(2,1)->first();

        EzyShoppingCart::add($productFirst['name'], $productQuantity, $productFirst['price']);
        EzyShoppingCart::add($productSecond['name'], $productQuantity, $productSecond['price']);

        $overallTotal += $productFirst['price'] * $productQuantity;
        $overallTotal += $productSecond['price'] * $productQuantity;

        $this->assertEquals($overallTotal, EzyShoppingCart::getTotal());
    }

    /** @test */
    public function add_product_twice_to_the_cart()
    {
        $product = EzyProduct::first();
        EzyShoppingCart::add($product['name'], 1, $product['price']);
        EzyShoppingCart::add($product['name'], 1, $product['price']);

        $this->assertEquals(2, EzyShoppingCart::GetContent()->first()->quantity);
    }

    /** @test */
    public function remove_product_from_the_cart()
    {
        $product = EzyProduct::first();
        $cartItem = EzyShoppingCart::add($product['name'], 10, $product['price']);
        EzyShoppingCart::remove($cartItem->id);

        $this->assertEquals(0, count(EzyShoppingCart::GetContent()));
    }

    /**
     * Assert an array structure is equal to the given array.
     *
     * @param array $structure
     * @param array $arrayData
     */
    protected function assertArrayStructure(array $structure, array $arrayData)
    {
        foreach ($arrayData as $arrayDataItem) {
            foreach ($structure as $structureArrayKey) {
                $this->assertCount(count($structure), array_keys($arrayDataItem));
                $this->assertArrayHasKey($structureArrayKey, $arrayDataItem, "Array doesn't contains " . $structureArrayKey . " as key");
            }
        }
    }
}

<?php

namespace Tests\Unit;

use App\Ezy\EzyProduct;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{

    /** @test */
    public function count_the_products()
    {
        $this->assertCount(count(EzyProduct::$productsArray), EzyProduct::all());
    }

    /** @test */
    public function check_the_product_structure()
    {
        $this->assertArrayStructure(['name', 'price'], EzyProduct::all()->toArray());
    }

    /**
     * Assert an array structure is equal to the given array.
     *
     * @param array $structure
     * @param array $arrayData
     */
    protected function assertArrayStructure(array $structure, array $arrayData)
    {
        foreach ($arrayData as $arrayDataItem) {
            foreach ($structure as $structureArrayKey) {
                $this->assertCount(count($structure), array_keys($arrayDataItem));
                $this->assertArrayHasKey($structureArrayKey, $arrayDataItem, "Array doesn't contains " . $structureArrayKey . " as key");
            }
        }
    }
}

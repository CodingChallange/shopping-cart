<?php

namespace App\Ezy;


class EzyProduct
{
    /**
     * @var array[]
     */
    public static $productsArray = [
        ["name" => "Sledgehammer", "price" => 125.75],
        ["name" => "Axe", "price" => 190.50],
        ["name" => "Bandsaw", "price" => 562.131],
        ["name" => "Chisel", "price" => 12.9],
        ["name" => "Hacksaw", "price" => 18.45],

    ];

    /**
     * Get the first product.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function first()
    {
        return self::getProductCollection()->first();
    }

    /**
     * Get all products.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function all()
    {
        return self::getProductCollection();
    }

    /**
     * Get the products collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getProductCollection()
    {
        return collect(self::$productsArray);
    }

    /**
     * Find a product by name.
     *
     * @param string $name
     * @return mixed|void
     */
    public static function find(string $name)
    {
        $product = self::getProductCollection()->firstWhere('name', $name);

        return $product ?: abort(404);
    }
}

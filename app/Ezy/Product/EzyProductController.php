<?php

namespace App\Http\Controllers;


use App\Ezy\EzyProduct;
use Illuminate\Support\Facades\View;

class EzyProductController extends Controller
{
    /**
     * Display a listing of the products.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return View::make('ezyCart.index')
            ->with('products', EzyProduct::all());
    }
}

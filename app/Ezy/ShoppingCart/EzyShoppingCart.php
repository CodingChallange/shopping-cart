<?php

namespace App\Ezy\ShoppingCart;

use App\Ezy\EzyProduct;
use App\Ezy\ShoppingCart\Repositories\EzyShoppingCartRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;

class EzyShoppingCart
{
    const DEFAULT_CART_INSTANCE = 'default';
    const CART_INSTANCE_PREFIX = 'ezy.cart';

    /**
     * @var string
     */
    private $instance;

    /**
     * @var EzyShoppingCartRepositoryInterface
     */
    private $repository;

    /**
     * @var Collection
     */
    private $content;

    /**
     * ShoppingCart constructor.
     *
     * @param EzyShoppingCartRepositoryInterface $repository
     */
    public function __construct(EzyShoppingCartRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->instance();
        $this->getContent();
    }

    /**
     * Get index view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return View::make('ezyCart.cart-index')
            ->with('cartItems', $this->content)
            ->with('total', $this->getTotal());
    }

    /**
     * Add a new item to the shopping cart or if the item is already exist in the cart, updates the quantity for
     * that item.
     *
     * @param string $itemName
     * @param int $quantity
     * @param float $price
     * @return EzyShoppingCartItem
     */
    public function add(string $itemName, int $quantity, float $price)
    {
        $cartItem = new EzyShoppingCartItem($itemName, $quantity, $price);

        if ($this->content->has($cartItem->id)) {
            $cartItem->quantity += $this->content->get($cartItem->id)->quantity;
            $cartItem->total = $cartItem->quantity * $cartItem->price;
        }

        $this->repository->createOrUpdate($this->instance, $cartItem);
        $this->getContent();

        return $cartItem;
    }

    /**
     * Remove an item from shopping cart.
     *
     * @param string $uniqueId
     * @return bool
     */
    public function remove(string $uniqueId)
    {
        if ($cartItem = $this->get($uniqueId)) {

            $this->content->pull($cartItem->id);
            $this->repository->remove($uniqueId, $this->instance);
            $this->getContent();

            return true;
        }

        return false;
    }

    /**
     * Check if an item with specified unique id is in shopping cart.
     *
     * @param string $uniqueId
     * @return bool
     */
    public function has(string $uniqueId)
    {
        return $this->content->has($uniqueId);
    }

    /**
     * Get the item with the specified unique id from shopping cart.
     *
     * @param $uniqueId
     * @return mixed
     */
    public function get($uniqueId)
    {
        return $this->content->get($uniqueId);
    }

    /**
     * Get shopping cart content.
     *
     * @return Collection
     */
    public function getContent()
    {
        return $this->content = collect($this->repository->all($this->instance));
    }

    /**
     * Set shopping cart instance.
     *
     * @param string|null $instance
     * @return string|null
     */
    public function instance(string $instance = null)
    {
        $this->instance = self::CART_INSTANCE_PREFIX . '.' . ($instance ?: self::DEFAULT_CART_INSTANCE);

        return $this;
    }

    /**
     * Get total price for the sopping cart.
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->content->sum(function (EzyShoppingCartItem $cartItem) {
            return $cartItem->getTotal();
        });
    }

    /**
     * Store the given item to the shopping cart.
     *
     * @param string $name
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(string $name)
    {
        $product = EzyProduct::find($name);
        $this->add($product['name'], 1, $product['price']);

        return redirect()->route('cart.index');
    }

    /**
     * Delete the given item from the cart.
     *
     * @param string $uniqueId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(string $uniqueId)
    {
        $this->remove($uniqueId);

        return redirect()->route('cart.index');
    }
}

<?php

namespace App\Ezy\ShoppingCart\Facades;

use Illuminate\Support\Facades\Facade;

class EzyShoppingCart extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ezy-shopping-cart';
    }
}

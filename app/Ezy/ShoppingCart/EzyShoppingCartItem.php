<?php

namespace App\Ezy\ShoppingCart;

use Illuminate\Support\Str;
use InvalidArgumentException;
use LengthException;

class EzyShoppingCartItem
{
    /**
     * The identifier of the cart item.
     *
     * @var int|string
     */
    public $id;

    /**
     * The name of the cart item.
     *
     * @var string
     */
    public $name;

    /**
     * The price of the cart item.
     *
     * @var float
     */
    public $price;

    /**
     * The quantity for this cart item.
     *
     * @var int
     */
    public $quantity;

    /**
     * The total for this cart item.
     *
     * @var float
     */
    public $total;

    /**
     * EzyShoppingCartItem constructor.
     *
     * @param string $itemName
     * @param int $quantity
     * @param float $price
     */
    public function __construct(string $itemName, int $quantity, float $price)
    {
        $this->validateCartItem($itemName, $quantity, $price);

        $this->id = $this->generateUniqueId($itemName);
        $this->name =  $itemName;
        $this->quantity =  $quantity;
        $this->price =  $price;
        $this->total =  $price * $quantity;
    }

    /**
     * Generate an unique ID for the shopping cart item.
     *
     * @param string $string
     * @return string
     */
    protected function generateUniqueId(string $string)
    {
        return md5($string);
    }

    /**
     * Get total price for the cart.
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->price * $this->quantity;
    }

    /**
     * Validates the given cart item.
     *
     * @param string $name
     * @param int $quantity
     * @param float $price
     *
     * @throws InvalidArgumentException|LengthException
     */
    public function validateCartItem(string $name, int $quantity, float $price)
    {
        if (empty($name)) {
            throw new InvalidArgumentException('Cart item name is invalid. Please provide a valid name.');
        }

        if (Str::length($name) > 255) {
            throw new LengthException('Cart item name can\'t be longer than 255 characters. Please provide a shorter name.');
        }

        if (!is_int($quantity) || strlen($quantity) < 0) {
            throw new InvalidArgumentException('Cart item quantity is invalid. Please provide a valid quantity.');
        }

        if (!is_numeric($price) || Str::length($price) < 0) {
            throw new InvalidArgumentException('Cart item price is invalid. Please provide a valid price.');
        }
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'total' => $this->total,
        ];
    }
}

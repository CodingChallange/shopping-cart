<?php

namespace App\Ezy\ShoppingCart;


use App\Ezy\ShoppingCart\Repositories\EzyShoppingCartRepository;

class EzyShoppingCartServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->app->bind('ezy-shopping-cart', function () {
            return new EzyShoppingCart(
                $this->app->make(
                    EzyShoppingCartRepository::class
                ));
        });
    }
}

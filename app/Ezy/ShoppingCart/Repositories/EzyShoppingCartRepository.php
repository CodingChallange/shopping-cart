<?php

namespace App\Ezy\ShoppingCart\Repositories;

use App\Ezy\ShoppingCart\EzyShoppingCartItem;


class EzyShoppingCartRepository implements EzyShoppingCartRepositoryInterface
{
    /**
     * Create or update the shopping cart.
     *
     * @param string $instanceName
     * @param EzyShoppingCartItem $cartItem
     */
    public function createOrUpdate(string $instanceName, EzyShoppingCartItem $cartItem)
    {
        session()->put([$this->getKey($cartItem->id, $instanceName) => $cartItem]);
    }

    /**
     * Get all records from shopping cart.
     *
     * @param string $instanceName
     * @return array|mixed
     */
    public function all(string $instanceName)
    {
        return session()->get($instanceName) ?: [];
    }

    /**
     * Remove shopping cart by its identifier and instance name.
     *
     * @param string $id
     * @param string $instanceName
     */
    public function remove(string $id, string $instanceName)
    {
        session()->pull($this->getKey($id, $instanceName));
    }

    /**
     * Get the key to store the shopping cart item.
     *
     * @param string $id
     * @param string $instanceName
     * @return string
     */
    public function getKey(string $id, string $instanceName)
    {
        return $instanceName . '.' . $id;
    }
}

<?php

namespace App\Ezy\ShoppingCart\Repositories;

use App\Ezy\ShoppingCart\EzyShoppingCartItem;

interface EzyShoppingCartRepositoryInterface
{
    /**
     * Create or update the shopping cart.
     *
     * @param string $instanceName
     * @param EzyShoppingCartItem $cartItem
     */
    public function createOrUpdate(string $instanceName, EzyShoppingCartItem $cartItem);

    /**
     * Get all records from shopping cart.
     *
     * @param string $instanceName
     * @return array|mixed
     */
    public function all(string $instanceName);

    /**
     * Remove shopping cart by its identifier and instance name.
     *
     * @param string $id
     * @param string $instanceName
     */
    public function remove(string $id, string $instanceName);

    /**
     * Get the key to store the shopping cart item.
     *
     * @param string $id
     * @param string $instanceName
     * @return string
     */
    public function getKey(string $id, string $instanceName);
}
